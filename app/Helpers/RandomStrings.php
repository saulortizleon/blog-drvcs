<?php
namespace App\Helpers;

class RandomStrings
{
	public static function StringGenerator()
	{
		$characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

		$string = "";

		for ($i=0; $i < 20; $i++)
		{
			$string .= $characters[rand(0,strlen($characters)-1)];
		}

		return $string;
	}
}