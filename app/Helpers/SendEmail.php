<?php
namespace App\Helpers;

use Illuminate\Http\Request;

use App\Model\TUser;

use Mail;
use Session;
use Redirect;

class SendEmail
{
	public function SendStringMail($string, $user)
    {
        Mail::send('user.sendemail',['string'=>$string],function($x) use ($user)
        {
            $x->from(env('MAIL_USERNAME'),'svs.com');
            $x->to($user->email, ($user->firstName).' '.($user->lastName))->subject('Código de recuperación  de svs.com');
        });
    }

    public function SendEmailConfirmation($user)
    {
        Mail::send('user/accountconfirmation',['userId' => $user->idUser ,'userFirstName'=>$user->firstName,'userLastName' => $user->lastName],function($x) use ($user)
        {
            $x->from(env('MAIL_USERNAME'),'svs.com');
            $x->to($user->email, ($user->firstName).' '.($user->lastName))->subject('enlace de confirmacion de svs.com');
        });
    }

    public function SendEmailUserContact($email,$usermessage,$names,$subject)
    {   
        Mail::send('user/emailcontact',['names'=>$names, 'usermessage'=>$usermessage,'subject'=>$subject],function($x)use($email,$subject) 
        {  
            $x->from($email);
            $x->to('saulortizleon322@gmail.com')->subject($subject);
        });

    }
 }