<?php
namespace App\Helpers;
use Illuminate\Database\Eloquent\Collection;
 class paginacion
{
	public function prepararPaginacion($consulta, $registrosPagina, $paginaActual)
	{
		$cantidadRegistrosConsiderar=$registrosPagina;
		$paginaActual=$paginaActual<=0 ? 1 : $paginaActual;
		$cantidadPaginas=ceil(($consulta->count())/$cantidadRegistrosConsiderar);
		$paginaActual=$paginaActual>$cantidadPaginas ? ($cantidadPaginas > 0 ? $cantidadPaginas : 1) : $paginaActual;
		$listaRegistros=$consulta->skip(($paginaActual*$cantidadRegistrosConsiderar)-$cantidadRegistrosConsiderar)->take($cantidadRegistrosConsiderar)->get();
		$cantidadPaginas=($cantidadPaginas==0 ? 1 : $cantidadPaginas);

		return ['listaRegistros' => $listaRegistros, 'paginaActual' => $paginaActual, 'cantidadPaginas' => $cantidadPaginas];
	}

	public function renderizarPaginacion($urlPagina, $cantidadPaginas, $paginaActual, $parametroBusqueda = null)
	{
		$urlPagina = url($urlPagina);
		$urlPaginaAnterior = $urlPagina . ($parametroBusqueda != null && $parametroBusqueda != '' ? '/' . ( $paginaActual > 1 ? $paginaActual - 1 : $paginaActual ) . '?q=' . $parametroBusqueda : '/' . ( $paginaActual > 1 ? $paginaActual - 1 : $paginaActual ));
		$urlPaginaSiguiente = $urlPagina . ($parametroBusqueda != null && $parametroBusqueda != '' ? '/' . ( $paginaActual < $cantidadPaginas ? $paginaActual + 1 : $paginaActual ) . '?q=' . $parametroBusqueda : '/' . ( $paginaActual < $cantidadPaginas ? $paginaActual + 1 : $paginaActual ));
		$urlPagina .= $parametroBusqueda != null && $parametroBusqueda != '' ? '/paginaActual?q=' . $parametroBusqueda : '/paginaActual';
		
		$seccionPaginacion = '<div class="row">
		<div class="col-sm-4">
			<div class="dataTables_paginate paging_simple_numbers pull-right" id="pagination_generated">
				<ul class="pagination" style="vertical-align:middle">
					<li class="paginate_button previous ' . ( $paginaActual == 1 ? 'disabled' : '' ) . '" id="pagination_generated_previus">
						<a href="' . $urlPaginaAnterior . '" tabindex="0">Anterior</a>
					</li>';

		$nextPage = '<li class="paginate_button next  ' . ( $paginaActual == $cantidadPaginas ? 'disabled' : '' ) . '" id="pagination_generated_next"><a href="' . $urlPaginaSiguiente . '" tabindex="0">Siguiente</a>
		</li>';

		$itemsPagination = '';

		if($cantidadPaginas > 10)
		{
			for($i = 1; $i <= 5 ; $i ++)
			{
				$itemsPagination .= '<li class="paginate_button ' . ($i == $paginaActual ? 'active' : '') . '"><a style="cursor:pointer;" href="' . ( str_replace('paginaActual', $i, $urlPagina) ) . '" tabindex="0">' . $i . '</a>
				</li>';
			}

			$itemsPagination .= '<li class="paginate_button"><a style="cursor:pointer;" tabindex="0">...</a>
			</li>';

			for($i = $cantidadPaginas - 5; $i <= $cantidadPaginas ; $i ++)
			{
				$itemsPagination .= '<li class="paginate_button ' . ($i == $paginaActual ? 'active' : '') . '"><a style="cursor:pointer;" href="' . ( str_replace('paginaActual', $i, $urlPagina) ) . '" tabindex="0">' . $i . '</a>
				</li>';
			}
		}
		else
		{
			for($i = 1; $i <= $cantidadPaginas ; $i ++)
			{
				$itemsPagination .= '<li class="paginate_button ' . ($i == $paginaActual ? 'active' : '') . '"><a style="cursor:pointer;" href="' . ( str_replace('paginaActual', $i, $urlPagina) ) . '" tabindex="0">' . $i . '</a>
				</li>';
			}
		}

		$seccionPaginacion .= $itemsPagination . $nextPage;

		return $seccionPaginacion;
	}
}
?>