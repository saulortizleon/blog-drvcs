<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;

use App\Helpers\RandomStrings;
use App\Helpers\Messages;
use App\Model\Tpublicacion;
use App\Model\Tagenda;
use App\Model\Tarchivos;
use App\Validation\Publicacion;

use Redirect;
use DB;
use Mail;
use Session;

class AdminController extends Controller
{
    public function listaAVisos()
    {
        $aviso= Tpublicacion::where('categoria','aviso')->orderBy('created_at', 'DESC')->paginate(3);

        return view('noticias/principal',['aviso'=>$aviso]);
    }
    public function admin()
    {
        return view('layout/admin');
    }

    public function adminNoticias()
    {
        $publicacion=Tpublicacion::where('estado','activo')->get();

        return view('admin/noticias/lista',['publicacion'=> $publicacion]);
    }
    public function insertarNoticia(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $publicacion = new Tpublicacion();
                $publicacion->idPublicacion = uniqid();
                $publicacion->titulo = $request->get('titulo');
                $publicacion->subtitulo = $request->get('subtitulo');
                $publicacion->contenido= $request->get('contenido');
                $publicacion->categoria= $request->get('categoria');
                $publicacion->embed= $request->get('embed');
                if($request->hasFile('imagen'))
                {
                    $extension = strtolower($request->file('imagen')->getClientOriginalExtension());
                    $request->file('imagen')->move(public_path().'/blog',$publicacion->idPublicacion.'.'.$extension );

                    $publicacion->imagen = $request->get('hiddenUrl').'/blog/'.$publicacion->idPublicacion.'.'.$extension;
                }
                if($request->hasFile('archivo'))
                {
                    $extension = strtolower($request->file('archivo')->getClientOriginalExtension());
                    $request->file('archivo')->move(public_path().'/doc',$publicacion->idPublicacion.'.'.$extension );

                    $publicacion->archivo = $request->get('hiddenUrl').'/doc/'.$publicacion->idPublicacion.'.'.$extension;
                }

                $publicacion->estado= $request -> get('estado');
                $dni=Session::get('t_usuario')[0];
                $publicacion->dni=$dni;

                $publicacion->save();
                DB::commit();

                return $messages->MessageCorrect('Noticias publicada','adm/noticias');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Registro de usuario incorrecto','adm/noticias');
            }
        }
        return view('admin/noticias/insertar');
    }
    public function editarNoticia(Request $request,Messages $messages, $idPublicacion=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $publicacion = Tpublicacion::find($request->get('idPublicacion'));
                $publicacion->titulo = $request->get('titulo');
                $publicacion->subtitulo = $request->get('subtitulo');
                $publicacion->contenido= $request->get('contenido');
                $publicacion->categoria= $request->get('categoria');
                $publicacion->embed= $request->get('embed');
                if($request->hasFile('imagen'))
                {
                    $extension = strtolower($request->file('imagen')->getClientOriginalExtension());
                    $request->file('imagen')->move(public_path().'/blog',$publicacion->idPublicacion.'.'.$extension );

                    $publicacion->imagen = $request->get('hiddenUrl').'/blog/'.$publicacion->idPublicacion.'.'.$extension;
                }
                if($request->hasFile('archivo'))
                {
                    $extension = strtolower($request->file('archivo')->getClientOriginalExtension());
                    $request->file('archivo')->move(public_path().'/doc',$publicacion->idPublicacion.'.'.$extension );

                    $publicacion->archivo = $request->get('hiddenUrl').'/doc/'.$publicacion->idPublicacion.'.'.$extension;
                }

                $publicacion->estado= $request -> get('estado');
                $dni=Session::get('t_usuario')[0];
                $publicacion->dni=$dni;

                $publicacion->save();
                DB::commit();

                return $messages->MessageCorrect('Noticias actualizada','adm/noticias');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('hubo un error','adm/noticias');
            }
        }
        $listaEditar = Tpublicacion::find($idPublicacion);
        return view('admin/noticias/editar',['listaEditar' => $listaEditar]);

    }
    public function insertarAgenda(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $agenda = new Tagenda();

                $agenda->idAgenda = uniqid();
                $agenda->nombre = $request->get('nombre');
                $agenda->fecha = $request->get('fecha');
                $agenda->hora= $request->get('hora');
                $agenda->lugar= $request->get('lugar');
                $agenda->estado= $request->get('estado');
                $agenda->tag= $request->get('tag');
                $dni=Session::get('t_usuario')[0];
                $agenda->dni=$dni;

                $agenda->save();
                DB::commit();

                return $messages->MessageCorrect('Agenda publicada','adm/agenda');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con la agenda','adm/agenda');
            }
        }
        return view('adm/agenda');
    }
    public function editarAgenda(Request $request,Messages $messages, $idAgenda = null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $agenda =  Tagenda::find($request->get('idAgenda'));

                $agenda->idAgenda = uniqid();
                $agenda->nombre = $request->get('nombre');
                $agenda->fecha = $request->get('fecha');
                $agenda->hora= $request->get('hora');
                $agenda->lugar= $request->get('lugar');
                $agenda->estado= $request->get('estado');
                $agenda->tag= $request->get('tag');
                $dni=Session::get('t_usuario')[0];
                $agenda->dni=$dni;

                $agenda->save();
                DB::commit();

                return $messages->MessageCorrect('Agenda publicada','adm/agenda');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con la agenda','adm/agenda');
            }
        }

 		$listAgenda = Tagenda::find($idAgenda);

 		return view('agenda/edit',['listAgenda' => $listAgenda]);
    }
    public function eliminarArchivo($idDoc, Messages $messages)
    {

        try
        {
            DB::beginTransaction();

            $archivo = Tarchivos::find($idDoc);

            $archivo->estado = "Inactivo";

            $archivo->save();

            DB::commit();
            return $messages->MessageCorrect('archivo  eliminada','adm/archivos');
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return $messages->MessageCorrect('Hubo un error, intente nuevamente','adm/archivos');
        }

    }
    public function eliminarNoticia($idNoticia, Messages $messages)
    {
        try
        {
            DB::beginTransaction();

            $noticia = Tpublicacion::find($idNoticia);

            $noticia->estado = "Inactivo";

            $noticia->save();

            DB::commit();


            return $messages->MessageCorrect('noticia  eliminada','adm/noticias');
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return $messages->MessageCorrect('Hubo un error, intente nuevamente','adm/noticias');
        }

    }
    public function eliminarAgenda($idAgenda, Messages $messages)
 	{
 		try
 		{
 			DB::beginTransaction();

 			$agenda = Tagenda::find($idAgenda);

	 		$agenda->estado = "Inactivo";

	 		$agenda->save();

	 		DB::commit();


	 		return $messages->MessageCorrect('Agenda eliminada','adm/agenda');
 		}
 		catch(\Exception $e)
 		{
 			DB::rollback();
	 		return $messages->MessageCorrect('Hubo un error, intente nuevamente','adm/agenda');
 		}

 	}
    public function adminAgenda()
    {
        $agenda=Tagenda::where('estado','activo')->get();

        return view('admin/agenda/lista',['agenda'=> $agenda]);
    }

    public function insertarArchivos(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $archivos = new Tarchivos();

                $archivos->idDoc = uniqid();
                if($request->hasFile('archivo'))
                {
                    $extension = strtolower($request->file('archivo')->getClientOriginalExtension());
                    $request->file('archivo')->move(public_path().'/archivos',$archivos->idDoc.'.'.$extension );

                    $archivos->extension = $request->get('hiddenUrl').'/archivos/'.$archivos->idDoc.'.'.$extension;
                }
                $archivos->nombre = $request->get('nombre');
                $archivos->estado= $request->get('estado');
                $archivos->categoria= $request->get('categoria');
                $dni=Session::get('t_usuario')[0];
                $archivos->dni= $dni;

                $archivos->save();
                DB::commit();

                return $messages->MessageCorrect('archivo publicado','adm/archivos');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con el archivo','adm/archivos');
            }
        }
        return view('adm/archivos');
    }

    public function adminArchivos()
    {
        $archivos=Tarchivos::where('estado','activo')->get();

        return view('admin/archivos/lista',['archivos'=> $archivos]);
    }

}