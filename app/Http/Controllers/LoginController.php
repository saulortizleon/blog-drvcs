<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use App\Model\Tusuario;
use App\Helpers\Messages;

use Session;
use DB;

class LoginController extends Controller
{
    public function actionLogin(Request $request, SessionManager $sessionManager,Messages $messages)
	{
		if($_POST)
        {
			$user = Tusuario::where('dni',$request->get('dni'))->first();

			$contrasena=$request->input('contrasena');
            if($user != null)
            {
                if($user->contrasena==md5($contrasena) && $user->estado=='activo')
                {
                    $datos = [$user->dni , $user->nombres, $user->estado];
                    $sessionManager->put('t_usuario',$datos);

                    return Redirect::to('adm/noticias');
                }
            }

            return $messages->MessageIncorrect('Datos de inicio de sesión incorrectos', 'adm/login');
        }

        return view('/');
	}
    public function actionLogOut(SessionManager $sessionManager)
	{
		$sessionManager->flush();

		return redirect('/');
    }
    public function ver()
    {
        return view('admin/login');
    }
    public function listaDocumentos()
    {
        $documento= Tpublicacion::where('categoria','documento')->orderBy('created_at', 'DESC')->paginate(3);

        return view('noticias/documentos',['documento'=>$documento]);
    }
}
?>