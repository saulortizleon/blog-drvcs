<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\ajax;
use Session;
use App\Model\Tpublicacion;
use App\Model\Tagenda;
use Response;

class NoticiasController extends Controller
{
    public function listaNoticias()
    {
        $noticias= Tpublicacion::where('categoria','noticia')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(12);

        return view('noticias/lista',['noticias'=>$noticias]);
    }
    public function listaDocumentos()
    {
        $documento= Tpublicacion::where('categoria','documento')->paginate(8);

        return view('noticias/documentos',['documento'=>$documento]);
    }
    public function verGaleria()
    {   $galeria= Tpublicacion::where('categoria','noticia')->orderBy('created_at', 'DESC')->paginate(4);

        return view('noticias/galeria',compact('galeria'));
    }
    public function romas()
    {   $romas= Tpublicacion::where('tag','romas')->orderBy('created_at', 'DESC')->paginate(2);

        return view('noticias/romas',compact('romas'));
    }
    public function directorio()
    {   $directorio= Tpublicacion::where('tag','directorio')->orderBy('created_at', 'DESC')->paginate(2);

        return view('noticias/directorio',compact('directorio'));
    }
    public function plan()
    {   $plan= Tpublicacion::where('tag','plan')->orderBy('created_at', 'DESC')->paginate(2);

        return view('noticias/plan',compact('plan'));
    }
    public function index(Request $request)
    {
        $publicacion=Tpublicacion::where([['estado','activo'],['categoria','noticia']])->orderBy('created_at', 'DESC')->paginate(3);
        $documento=Tpublicacion::where('categoria','documento')->orderBy('created_at', 'DESC')->paginate(1);

        return view('noticias/principal',['publicacion'=> $publicacion],['documento'=> $documento]);
    }

    public function documento(Request $request)
    {
        if($request->ajax()){
            $documento=   Tpublicacion::where('categoria','documento')->orderBy('created_at', 'DESC')->paginate(3);

            return Response::json($documento);
        }
        return "HTTP";
       /*

        return Response::json($agenda);*/
    }
    public function listaAviso(Request $request)
    {
        if($request->ajax()){
            $aviso=   Tpublicacion::where('categoria','aviso')->orderBy('created_at', 'DESC')->paginate(3);

            return Response::json($aviso);
        }
        return "HTTP";
       /*

        return Response::json($agenda);*/
    }
    public function agenda(Request $request)
    {
        if($request->ajax()){
            $agenda= Tagenda::where('estado','activo')->orderBy('created_at','DESC')->paginate(1);
            return Response::json($agenda);
        }
        return "HTTP";
       /*

        return Response::json($agenda);*/
    }

    public function detalle(Request $request, $id)
    {
    	$detalle=Tpublicacion::find($id);

    	return view('noticias.detalle',['detalle' => $detalle]);
    }
    public function institucional()
    {
        return view('noticias/institucional');
    }
    public function documentos()
    {
        return view('noticias/documentos');
    }
    public function contacto()
    {
        return view('noticias/contacto');
    }
    public function oficinas()
    {
        return view('noticias/oficinas');
    }
    public function transparencia()
    {
        return view('noticias/transparencia');
    }
}