<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tagenda extends Model
{
    protected $table='t_agenda';
    protected $primaryKey='idAgenda';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tusuario()
	  {
		return $this->belongTo('App\Model\Tusuario', 'dni');
    }

}