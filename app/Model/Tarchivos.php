<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tarchivos extends Model
{
    protected $table='t_doc';
    protected $primaryKey='idDoc';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tusuario()
	{
		return $this->belongTo('App\Model\Tusuario', 'dni');
    }
}