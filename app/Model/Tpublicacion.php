<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TPublicacion extends Model
{
    protected $table='t_publicacion';
    protected $primaryKey='idPublicacion';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tusuario()
	  {
		return $this->belongTo('App\Model\Tusuario', 'dni');
    }

}