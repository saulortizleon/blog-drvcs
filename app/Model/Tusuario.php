<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tusuario extends Model
{
    protected $table='t_usuario';
    protected $primaryKey='dni';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tpublicacion()
	{
		return $this->hasMany('App\Model\Tpublicacion', 'idPublicacion');
    }
    
    public function Tagenda()
	{
		return $this->hasMany('App\Model\Tagenda', 'idAgenda');
    }
    public function Tarchivos()
	{
		return $this->hasMany('App\Model\Tarchivos', 'idDoc');
	}
}