-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_agenda`
--

DROP TABLE IF EXISTS `t_agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_agenda` (
  `idAgenda` char(13) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `fecha` varchar(150) DEFAULT NULL,
  `hora` varchar(150) DEFAULT NULL,
  `lugar` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dni` char(8) NOT NULL,
  PRIMARY KEY (`idAgenda`),
  KEY `fk_t_agenda_t_usuario1` (`dni`),
  CONSTRAINT `fk_t_agenda_t_usuario1` FOREIGN KEY (`dni`) REFERENCES `t_usuario` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_agenda`
--

LOCK TABLES `t_agenda` WRITE;
/*!40000 ALTER TABLE `t_agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_doc`
--

DROP TABLE IF EXISTS `t_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_doc` (
  `idDoc` char(13) NOT NULL,
  `extension` text,
  `nombre` varchar(45) DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dni` char(8) NOT NULL,
  `estado` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idDoc`),
  KEY `fk_t_doc_t_usuario1` (`dni`),
  CONSTRAINT `fk_t_doc_t_usuario1` FOREIGN KEY (`dni`) REFERENCES `t_usuario` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_doc`
--

LOCK TABLES `t_doc` WRITE;
/*!40000 ALTER TABLE `t_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_publicacion`
--

DROP TABLE IF EXISTS `t_publicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_publicacion` (
  `idPublicacion` char(13) NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `subtitulo` varchar(150) DEFAULT NULL,
  `contenido` longtext,
  `categoria` varchar(45) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  `imagen` text,
  `archivo` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dni` char(8) NOT NULL,
  PRIMARY KEY (`idPublicacion`),
  KEY `fk_t_publicacion_t_usuario` (`dni`),
  CONSTRAINT `fk_t_publicacion_t_usuario` FOREIGN KEY (`dni`) REFERENCES `t_usuario` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_publicacion`
--

LOCK TABLES `t_publicacion` WRITE;
/*!40000 ALTER TABLE `t_publicacion` DISABLE KEYS */;
INSERT INTO `t_publicacion` VALUES ('5da72f9b5b86d','Campaña de recojo de basura en las cuencas y riachuelos del sector de San Isidro','Se realizó el  21 de septiembre del 2019','La Dirección Regional de Vivienda, Construcción y Saneamiento, participó el día sábado 21 de septiembre en la campaña de recojo de basura en las cuencas y riachuelos del sector de San Isidro hasta Nuevo Horizonte de la ciudad de Abancay. Por el Día Mundial de la Limpieza.\r\n\r\n#AllinKawsanapaq','noticia','activo','/blog/5da72f9b5b86d.jpg',NULL,'2019-10-16 14:56:27','2019-10-16 14:56:27','75394839'),('5da7301914552','IV taller dirigido a las Áreas Técnica Municipales 2019 - Abancay','Se realizó el día 01 de octubre con una masiva participación','Gobierno Regional de Apurímac mediante la Dirección Regional de Vivienda, Construcción y Saneamiento, realizaron el día 01 de octubre del 2019 el IV taller dirigido a las Áreas Técnica Municipales, con la finalidad de articular esfuerzos para la mejora de la prestación de los servicios de Saneamiento.\r\n\r\n#AllinKawsanapaq','noticia','Inactivo',NULL,NULL,'2019-10-16 14:58:33','2019-10-16 14:58:47','75394839'),('5da730506d2cf','IV taller dirigido a las Áreas Técnica Municipales 2019 - Abancay','Se realizó el día 01 de octubre con una masiva participación','Gobierno Regional de Apurímac mediante la Dirección Regional de Vivienda, Construcción y Saneamiento, realizaron el día 01 de octubre del 2019 el IV taller dirigido a las Áreas Técnica Municipales, con la finalidad de articular esfuerzos para la mejora de la prestación de los servicios de Saneamiento.\r\n \r\n #AllinKawsanapaq','noticia','activo','/blog/5da730506d2cf.jpg',NULL,'2019-10-16 14:59:28','2019-10-16 14:59:28','75394839'),('5da731acd87dd','Asistencia a la primera audiencia pública regional Apurímac 2019','Ralizada en el hermoso y turístico distrito de Pacucha','se desarrolló con gran éxito, tanto por la importante presencia y participación de Alcaldes, representantes de los gremios apurimeños, funcionarios de las instituciones diferentes del ámbito regional y resaltar la mayoritaria presencia del pueblo andahuaylino, dónde nuestro Gobernador Regional hizo una brillante exposición de todo lo actuado por su gestión, en ésta importante cita y espacio de diálogo democrático resaltó la presencia de la gerente de la Sub Región de Chincheros, qué se hizo presente acompañada de sus funcionarios y equipo de imagen y comunicación institucional.Cabe señalar la didáctica y técnica exposición del Lic. Baltazar Lantaron Nuñez dejó más qué satisfechos a los asistentes qué al finalizar, éste espacio democrático fue muy aplaudido y requerido por la prensa regional cómo también por el pueblo en general. Al finalizar el mandatario regional realizó un llamado a trabajar con eficiencia y honestidad y al gritó de kausachum Apurimac dio por finalizada esta cita con su pueblo.','noticia','Inactivo','/blog/5da731acd87dd.jpg',NULL,'2019-10-16 15:05:16','2019-10-16 15:07:32','75394839'),('5da73221a4b3d','Asistencia a la primera audiencia pública regional Apurímac 2019','Ralizada en el hermoso y turístico distrito de Pacucha','se desarrolló con gran éxito, tanto por la importante presencia y participación de Alcaldes, representantes de los gremios apurimeños, funcionarios de las instituciones diferentes del ámbito regional y resaltar la mayoritaria presencia del pueblo andahuaylino, dónde nuestro Gobernador Regional hizo una brillante exposición de todo lo actuado por su gestión, en ésta importante cita y espacio de diálogo democrático resaltó la presencia de la gerente de la Sub Región de Chincheros, qué se hizo presente acompañada de sus funcionarios y equipo de imagen y comunicación institucional.Cabe señalar la didáctica y técnica exposición del Lic. Baltazar Lantaron Nuñez dejó más qué satisfechos a los asistentes qué al finalizar, éste espacio democrático fue muy aplaudido y requerido por la prensa regional cómo también por el pueblo en general. Al finalizar el mandatario regional realizó un llamado a trabajar con eficiencia y honestidad y al gritó de kausachum Apurimac dio por finalizada esta cita con su pueblo.','noticia','activo','/blog/5da73221a4b3d.jpg',NULL,'2019-10-16 15:07:13','2019-10-16 15:07:13','75394839'),('5da7362fb68fe','HORARIOS DE ATENCIÓN DE LA DRVCS APURÍMAC',NULL,NULL,'aviso','activo','/blog/5da7362fb68fe.png',NULL,'2019-10-16 15:24:31','2019-10-16 15:24:31','75394839'),('5db8a06e8cdd6','aaa','aaa','aa','documento','activo',NULL,'/doc/5db8a06e8cdd6.pdf','2019-10-29 20:26:22','2019-10-29 20:26:59','75394839'),('5db8a0a3632b5','aaa','aaa','aaa','noticia','activo','/blog/5db8a0a3632b5.jpg',NULL,'2019-10-29 20:27:15','2019-10-29 20:27:15','75394839'),('5db8a31482360','AAAAAAAA','AAAAAAAAAAA','AAAAAAAAAAA','noticia','activo',NULL,'/doc/5db8a31482360.pdf','2019-10-29 20:37:40','2019-10-29 20:37:40','75394839');
/*!40000 ALTER TABLE `t_publicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_usuario`
--

DROP TABLE IF EXISTS `t_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_usuario` (
  `dni` char(8) NOT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `avatar` text,
  `descripcion` text,
  `contrasena` char(32) DEFAULT NULL,
  `correo` varchar(40) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_usuario`
--

LOCK TABLES `t_usuario` WRITE;
/*!40000 ALTER TABLE `t_usuario` DISABLE KEYS */;
INSERT INTO `t_usuario` VALUES ('31363829','Justino Cuipa',NULL,'Imagen Instituciona','01225c2db779cb2345cd56400e375888',NULL,NULL,NULL,NULL,NULL,'activo'),('75394839','Saul Ortiz',NULL,'Imagen Institucional','0af5df9e25806891c79c30496884781b',NULL,NULL,NULL,NULL,NULL,'activo');
/*!40000 ALTER TABLE `t_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-30 14:42:31
