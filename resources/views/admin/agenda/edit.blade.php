<script>
    $('#modalEdit').modal('show');
</script>
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"   class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
                <h4 class="modal-title">Editar agenda</h4>
            </div>
            <div class="modal-body">
                <form id="frmAgenda" name="frmAgenda" action="{{url('agenda/edit')}}" method="post" enctype="multipart/form-data">  
                    {{csrf_field()}}
                    <input type="hidden" name="idAgenda" value="{{$listagenda->idAgenda}}">
                    <div class="form-group">
                        <label for="description">Nombre</label>
                        <input type="text"  class="form-control" id="nombre" name="nombre" value="{{$listagenda->nombre}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Fecha</label>
                        <input type="text"  class="form-control" id="fecha" name="fecha" value="{{$listagenda->fecha}}"  >
                    </div>
                    <div class="form-group">
                        <label for="description">hora</label>
                        <input type="text"  class="form-control" id="hora" name="hora" value="{{$listagenda->hora}}" >
                    </div>
                    <div class="form-group">
                        <label for="description">lugar</label>
                        <input type="text"  class="form-control" id="lugar" name="lugar" value="{{$listagenda->lugar}}">
                    </div>
                    <b> Estado :  </b>
                    <div class="form-group">                                                  
                            <label>
                            <input class="minimal" type="radio" name="estado" value="{{$listagenda->estado}}"  value="activo" checked>
                            Activo
                            </label>
                            <label>
                            <input class="minimal" type="radio" name="estado" value="{{$listagenda->estado}}" value="inactivo">
                            Inactivo
                            </label>                                
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>