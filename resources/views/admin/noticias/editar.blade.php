@extends('layout.admin')
@section('section')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">editar publicación
                    </h3>
                    <a type="button" class="btn btn-danger pull-right" href="{{ url('adm/noticias') }}"><i class="fa fa-arrow-left"></i> atras</a>
                    <hr>
                    <form id="frmPublicacion" name="frmPublicacion" action="{{url('noticias/editar')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="idPublicacion" value="{{ $listaEditar->idPublicacion }}" >
                            <div class="form-group">
                                <label for="description">Título</label>
                                <input type="text"  class="form-control" id="titulo" name="titulo" value="{{ $listaEditar->titulo }}"  >
                            </div>
                            <div class="form-group">
                                <label for="description">Subtítulo</label>
                                <input type="text"  class="form-control" id="subtitulo" name="subtitulo" value="{{$listaEditar->subtitulo}}">
                            </div>
                            <div class="form-group">
                                    <div class="box">
                                        <div class="box-header">
                                        <h3 class="box-title">Editar contenido de la publicación
                                        </h3>
                                        <!-- tools box -->
                                        <div class="pull-right box-tools">
                                        </div>
                                        <!-- /. tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body pad">

                                            <textarea class="textarea" id="contenido" name="contenido" placeholder="Escribir aquí..."
                                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$listaEditar->contenido}}</textarea>

                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="level" >Categoría</label>
                                <select name="categoria" class="form-control" id="categoria">
                                    <option  value="evento" {{$listaEditar->categoria =="evento" ? "selected" : ""}}>Evento</option>
                                    <option  value="aviso" {{$listaEditar->categoria =="aviso" ? "selected" : ""}}>Comunicado</option>
                                    <option  value="noticia" {{$listaEditar->categoria =="noticia" ? "selected" : ""}}>Nota de prensa</option>
                                    <option  value="documento" {{$listaEditar->categoria =="documento" ? "selected" : ""}}>Documento</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="level" >Tema </label>
                                <select name="tag" class="form-control" id="tag">
                                    <option  value="romas" {{$listaEditar->tag =="romas" ? "selected" : ""}}>Romas</option>
                                    <option  value="atm" {{$listaEditar->tag =="atm" ? "selected" : ""}}>ATM</option>
                                    <option  value="vivienda" {{$listaEditar->tag =="vivienda" ? "selected" : ""}}>Viviendas Saludables</option>
                                    <option  value="jass" {{$listaEditar->tag =="jass" ? "selected" : ""}}>JASS</option>
                                    <option  value="urbanismo" {{$listaEditar->tag =="urbanismo" ? "selected" : ""}}>Urbanismo</option>
                                    <option  value="convocatoria" {{$listaEditar->tag =="convocatoria" ? "selected" : ""}}>Convocatorias</option>
                                    <option  value="resolucion" {{$listaEditar->tag =="resolucion" ? "selected" : ""}}>Resolucion</option>
                                    <option  value="participacion" {{$listaEditar->tag =="participacion" ? "selected" : ""}}>Participaciones</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen</label>
                                <input type="file" id="imagen" name="imagen">
                            </div>
                            <div class="form-group">
                                    <label for="exampleInputFile">Subir documento</label>
                                    <input type="file" id="archivo" name="archivo">
                                </div>
                            <div class="form-group">
                                    <label for="embed">Subir Galería : </label>
                                    <input type="text" class="form-control" id="embed" name="embed" value="{{$listaEditar->embed}}">
                            </div>
                            <b> Estado :  </b>
                            <div class="form-group">
                                    <label>
                                    <input class="minimal" type="radio" name="estado"  value="activo" checked>
                                    Activo
                                    </label>
                                    <label>
                                    <input class="minimal" type="radio" name="estado" value="inactivo">
                                    Inactivo
                                    </label>
                            </div>
                            <div class="form-group">

                                    <input type="submit" value="Guardar" class="btn btn-primary">
                            </div>

                    </form>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection