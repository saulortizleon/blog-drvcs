@extends('layout.admin')
@section('section')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Agregar publicación
                    </h3>
                    <a type="button" class="btn btn-danger pull-right" href="{{ url('adm/noticias') }}"><i class="fa fa-arrow-left"></i> atras</a>
                    <hr>
                    <form id="frmPublicacion" name="frmPublicacion" action="{{url('noticias/insertar')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="description">Título</label>
                                <input type="text"  class="form-control" id="titulo" name="titulo" required="required">
                            </div>
                            <div class="form-group">
                                <label for="description">Subtítulo</label>
                                <input type="text"  class="form-control" id="subtitulo" name="subtitulo"  required="required">
                            </div>
                            <div class="form-group">
                                    <div class="box">
                                        <div class="box-header">
                                        <h3 class="box-title">Agregar contenido de la publicación
                                        </h3>
                                        <!-- tools box -->
                                        <div class="pull-right box-tools">
                                        </div>
                                        <!-- /. tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body pad">

                                            <textarea class="textarea" id="contenido" name="contenido" placeholder="Escribir aquí..."
                                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ></textarea>

                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="level" >Categoría</label>
                                <select name="categoria" class="form-control" id="categoria">
                                    <option  value="evento">Evento</option>
                                    <option  value="aviso">Comunicado</option>
                                    <option  value="noticia">Nota de prensa</option>
                                    <option  value="documento">Documento</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="level" >Tema </label>
                                <select name="tag" class="form-control" id="tag">
                                    <option  value="romas">Romas</option>
                                    <option  value="atm">ATM</option>
                                    <option  value="vivienda">Viviendas Saludables</option>
                                    <option  value="jass">JASS</option>
                                    <option  value="urbanismo">Urbanismo</option>
                                    <option  value="convocatoria">Convocatorias</option>
                                    <option  value="resolucion">Resolucion</option>
                                    <option  value="participacion">Participaciones</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen</label>
                                <input type="file" id="imagen" name="imagen">
                            </div>
                            <div class="form-group">
                                    <label for="exampleInputFile">Subir documento en <B style="color: red;">formato PDF</B> :</label>
                                    <input type="file" id="archivo" name="archivo">
                                </div>
                            <div class="form-group">
                                    <label for="embed">Subir Galería : </label>
                                    <input type="text" class="form-control" id="embed" name="embed">
                            </div>
                            <b> Estado :  </b>
                            <div class="form-group">
                                    <label>
                                    <input class="minimal" type="radio" name="estado"  value="activo" checked>
                                    Activo
                                    </label>
                                    <label>
                                    <input class="minimal" type="radio" name="estado" value="inactivo">
                                    Inactivo
                                    </label>
                            </div>
                            <div class="form-group">

                                    <input type="submit" value="Guardar" class="btn btn-primary">
                            </div>

                    </form>
            </div>
        </div>
        </div>
    </div>
</div>
$(document).ready(function() {
    $('#frmPublicacion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            imagen:{
                validators: {
                    notEmpty: {
                        message: 'El Verificable es requerido'
                    },
                    file: {
                        maxSize: 2097152,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 2MB'
                    }
                }
            }

        }
    });
 });
</script>
@endsection