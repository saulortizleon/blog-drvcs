@extends('layout.admin')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE NOTICIAS</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Administración</a></li>
        <li class='active'>Noticias</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Lista de noticias</h3>
                    <button class="btn btn-success btn-sm pull-right" onclick="insertar()">Agregar noticia</button>
                </div>
                <div class="box-body">
                    <form action="{{url('centrop/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> Titulo</th>
                                <th> Categoria</th>
                                <th> Fecha publicación</th>
                                <th> Estado</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($publicacion as $item)
                            <tr>
                                <td>{{$item->titulo}}</td>
                                <td>{{$item->categoria}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>{{$item->estado}}</td>
                                <td>
                                <a title="Editar" href="#" class="btn btn-primary btn-xs" onclick="editarNoticia('{{$item->idPublicacion}}');"><i class="fa fa-edit"></i></a>
                                 <a title="Eliminar" href="#" class="btn btn-danger btn-xs" onclick="eliminarNoticia('{{$item->idPublicacion}}');"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function insertar()
	{
			window.location = "{{url('noticias/insertar')}}";
    }
    function editarNoticia(idPublicacion)
	{
		if(confirm('Está seguro de editar la publicación?'))
		{
			window.location = "{{url('noticias/editar')}}/"+idPublicacion;
		}
	}
</script>
@endsection