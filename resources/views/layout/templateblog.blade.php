<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Direccion Regional de Vivienda, Construcción y Saneamiento Apurimac">
    <meta name="author" content="DRVCS APURIMAC">
    <meta name="keywords" content="Saneamiento Apurimac, Agua Apurimac, Vivienda Apurímac, Construcción Apurimac, Agua de calidad, Cloración de agua, Proyectos de Agua" , "Vivienda Apurimac", "Saneamiento basico", "Gestion Agua", "gestion saneamiento", "ATM">
    <meta name="copyright" content="DRVCS APURIMAC">
    <a hreflang="es">
    <title>DRVCS Apurimac</title>
    <link href="{{asset('admin/vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{asset('admin/css/modern-business.css')}}" rel="stylesheet">
    <link href="{{asset('admin/dist/css/skins/_all-skins.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('formvalidation/formValidation.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "GovernmentOrganization",
          "name": "Dirección Regional de Vivienda Construcción y Saneamiento",
          "alternateName": "DRVCS APURIMAC",
          "url": "http://drvcs.regionapurimac.gob.pe",
          "logo": "http://drvcs.regionapurimac.gob.pe/layout/images/drvcs.png",
          "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "(083) 322837",
            "contactType": "billing support",
            "areaServed": "PE",
            "availableLanguage": "es"
          },
          "sameAs": "https://www.facebook.com/drvcsapurimacoficial"
        }
    </script>

</head>
<style>
        .btn-group-xs > .btn, .btn-xs {
          padding: .25rem .4rem;
          font-size: .875rem;
          line-height: .5;
          border-radius: .2rem;
        }

        .dropdown:hover>.dropdown-menu {
            display: block;
        }

        .tg-site-header .tg-site-header-top {
            background-color: #bd1f25;
            background-repeat: repeat;
            background-position: center center;
            background-size: contain;
            background-attachment: scroll;
        }

        img:hover {
            opacity: 0.9;
            filter: alpha(opacity=50);
            /* For IE8 and earlier */
        }

        #agenda {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #avatar {
            vertical-align: middle;
            width: 130px;
            height: 150px;
            border-radius: 50%;
        }
        #login {
            border-style: solid;
        }
          .example-8 {
              height:auto !important;
              width:160px !important;
          }

        @media  only screen and (max-width: 767px) {

        #ocultar {
            display: none;
        }
        @media  only screen and (max-width: 900px) {

            #ocultar {
                display: none;
            }
        }
        @media  only screen and (max-width: 500px) {

        #ocultar {
            display: none;
        }
        }
        #facebook {
            overflow:hidden;
            padding-bottom:56.25%;
            position:relative;
            height:0;
        }

        #facebook-responsive iframe {
            left:0;
            top:0;
            height:200%;
            width:100%;
            position:absolute;
        }
        .list-group-hover .list-group-item:hover {
            background-color: #f5f5f5;
        }
</style>
<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v5.0&appId=332640044163509&autoLogAppEvents=1"></script>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary fixed-top">
        <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}"> <img class="example-8" src="http://drvcs.regionapurimac.gob.pe/layout/images/drvcs.png"></img> </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}"><i class="fa fa-home"></i> Inicio</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-institution"></i> Nuestra Institución
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/institucional">Misión y Visión</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/objetivos">Objetivos</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/base-legal">Base legal</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/palabra">Palabras del Director(a)</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/funciones">Funciones</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/directorio">Directorio</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/funciones">Organigrama</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-group"></i> Direcciones
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="{{ url('vivienda-urbanismo') }}">Vivienda y Urbanismo</a>
                            <a class="dropdown-item" href="{{ url('construccion-saneamiento') }}">Construcción y Saneamiento</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="http://drvcs.regionapurimac.gob.pe/documentos" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-folder-o"></i> Transparencia
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/documentos">Documentos publicados</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/saneamiento">Convocatorias</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/documentos">Documentos de Gestión</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-newspaper-o"></i> Prensa
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/videos"><i class="fa fa-video-camera"></i> Videos</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/galeria"><i class="fa fa-photo"></i> Fotos</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/galeria"><i class="fa fa-bullhorn "></i> Comunicados</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/noticias"><i class="fa fa-sticky-note"></i> Noticias</a>
                            <a class="dropdown-item" href="http://drvcs.regionapurimac.gob.pe/vivo"><i class="fa fa-tv"></i> En Vivo</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://drvcs.regionapurimac.gob.pe/contacto"><i class="fa fa-phone"></i> Contáctenos</a>
                    </li>
                </ul>
                <ul class="navbar-nav navbar-right">
                    <li class="nav-item">
                        <a class=" nav-link btn btn-success btn-sm" type="button" href="http://drvcs.regionapurimac.gob.pe/adm/login">Ingresar</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card my-4">
                    <div class="card-body">
                        <center>
                            <img id="avatar" src="{{asset('img/eva.jpg')}}" alt="">  <br>
                             <i style="font-size:14px">Ing. Evangelina G. Lopez Contreras</i>
                             <label style="font-size:12px; color:#007bff ; font-weight: bold;">Directora Regional de Vivienda Construcción y Saneamiento Apurímac</label>
                        </center>
                    </div>
                </div>
                <div class="card my-4"  id="ocultar2" >
                    <h6  class="card-header  bg-primary" style="color:white">Agenda</h6>

                        <div id="listaAgenda"></div>

                </div>
                <div class="card my-4">
                        <ul class="list-group">
                                <li class="list-group-item "><a href="{{ url('noticias') }}"><i class="fa fa-sticky-note-o"></i>  Notas de prensa</a> </li>
                                <li class="list-group-item "><a href="{{ url('documuentos') }}"><i class="fa fa-file"></i> Documentos publicados</a> </li>
                                <li class="list-group-item "><a href="{{ url('comunicados') }}"><i class="fa fa-bullhorn"></i> Comunicados Oficiales</a> </li>
                                <li class="list-group-item "><a href="{{ url('directorio') }}"><i class="fa fa-phone-square"></i> Directorio</a> </li>
                                <li class="list-group-item "><a href="{{ url('videos') }}"><i class="fa fa-video-camera"></i> Videos</a> </li>
                                <li class="list-group-item "><a href="{{ url('galeria') }}"><i class="fa fa-photo"></i> Galería </a> </li>
                            </ul>
                </div>
            </div>
            <div class="col-md-9">
                    @yield('section')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-sm-12 portfolio-item">
                <div class="card h-100">
                    <a href="https://www.gob.pe/vivienda"><img class="card-img-top" src="http://drvcs.regionapurimac.gob.pe/img/vivienda.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12 portfolio-item">
                <div class="card h-100">
                    <a href="http://diresaapurimac.gob.pe"><img class="card-img-top" src="http://drvcs.regionapurimac.gob.pe/img/diresa.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12 portfolio-item">
                <div class="card h-100">
                    <a href="https://www.sunass.gob.pe/websunass/"><img class="card-img-top" src="http://drvcs.regionapurimac.gob.pe/img/sunass.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12 portfolio-item">
                <div class="card h-100">
                    <a href="http://www.regionapurimac.gob.pe/"><img class="card-img-top" src="http://drvcs.regionapurimac.gob.pe/img/region.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12 portfolio-item">
                <div class="card h-100">
                    <a href="http://www.midis.gob.pe/fed/"><img class="card-img-top" src="http://drvcs.regionapurimac.gob.pe/img/fed.png" alt=""></a>
                </div>
            </div>
                <div class="col-lg-2 col-sm-12 portfolio-item">
                <div class="card h-100">
                    <a href="https://www.ana.gob.pe/"><img class="card-img-top" src="http://drvcs.regionapurimac.gob.pe/img/ana.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <!-- Footer -->
    <footer class="page-footer font-small mdb-color pt-8 bg-primary" style="background-color:#007bff">

        <!-- Footer Links -->
        <div class="container text-center text-md-left">

            <!-- Footer links -->
            <div class="row text-center text-md-left mt-3 pb-3">

                <!-- Grid column -->
                <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold" style="color:#79fff4;">DRVCS APURÍMAC</h6>
                    <p style="color:white;">La Dirección Regional de Vivienda, Construcción y Saneamiento tiene competencia en materia de saneamiento, y como tal le corresponde planificar, formular y evaluar los planes y políticas regionales en concordancia con los planes de
                        desarrollo de los gobiernos locales y ejecutar las políticas nacionales y sectoriales conducentes para Lograr el acceso universal, sostenible y de calidad a los servicios de saneamiento.</p>
                </div>
                <!-- Grid column -->

                <hr class="w-100 clearfix d-md-none">

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold" style="color:#79fff4;"> Nosotros</h5>
                        <p>
                            <a style="color:white;" href="#!"><i class="fa fa fa-hand-o-right"></i> Presentación </a>
                        </p>
                        <p>
                            <a style="color:white;" href="#!"><i class="fa fa fa-hand-o-right"></i> Objetivos </a>
                        </p>
                        <p>
                            <a style="color:white;" href="#!"><i class="fa fa fa-hand-o-right"></i> Misión y Visión</a>
                        </p>
                        <p>
                            <a style="color:white;" href="#!"><i class="fa fa fa-hand-o-right"></i> Funciones</a>
                        </p>
                </div>
                <!-- Grid column -->

                <hr class="w-100 clearfix d-md-none">

                <!-- Grid column -->
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold" style="color:#79fff4;">Servicios Online</h6>
                    <p>
                        <a style="color:white;" href="#!"><i class="fa fa-check"></i> Notas de Prensa</a>
                    </p>
                    <p>
                        <a style="color:white;" href="#!"><i class="fa fa-check"></i> Techo Propio</a>
                    </p>
                    <p>
                        <a style="color:white;" href="#!"><i class="fa fa-check"></i> Directorio</a>
                    </p>
                    <p>
                        <a style="color:white;" href="#!"><i class="fa fa-check"></i> Trámites</a>
                    </p>
                    <p>
                        <a style="color:white;" href="#!"><i class="fa fa-check"></i> Documento</a>
                    </p>
                    <p>
                        <a style="color:white;" href="#!"><i class="fa fa-check"></i> Convocatorias</a>
                    </p>
                </div>

                <!-- Grid column -->
                <hr class="w-100 clearfix d-md-none">

                <!-- Grid column -->
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold" style="color:#79fff4;">Contacto</h6>
                    <p style="color:white;">
                        <i style="color:white;" class="fa fa-home "></i> Jr. Lima 637, Abancay Apurímac 03001
                    </p>
                    <p style="color:white;">
                        <i style="color:white;" class="fa fa-envelope  "></i> drvcsapurimac@hotmail.com</p>
                    <p style="color:white;">
                        <i style="color:white;" class="fa fa-phone "></i> (083) 322837</p>
                    <button type="button" class="btn btn-danger btn-sm"> <i class="fa fa-sitemap" style=" text-align: center;"> </i> Mapa de sitio </button>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Footer links -->

            <hr>

            <!-- Grid row -->
            <div class="row d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-7 col-lg-8">

                    <!--Copyright-->
                    <center>
                        <p style="color:white"> © 2019 Copyright : <strong>Dirección regional de Vivienda, Construcción y Saneamiento Apurímac</strong>
                    </center>
                    <a href="{{url('/')}}">

                    </a>
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-5 col-lg-4 ml-lg-0">

                    <!-- Social buttons -->
                    <div class="text-center text-md-right">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-2">
                                    <i class="fa fa-facebook-f" style="color:white"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-2">
                                    <i class="fa fa-twitter" style="color:white"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-2">
                                    <i class="fa fa-youtube" style="color:white"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

    </footer>
    <script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script src="{{asset('formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('formvalidation/bootstrap.validation.min.js')}}"></script>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0&appId=332640044163509&autoLogAppEvents=1"></script>
    <script>
        $(document).ready(function() {
            $('.carousel').carousel({
                interval: 3000
            });
        });
        $(document).ready(function() {
            $.ajax({
                url: 'agenda',
                type: 'get',
                success: function(respuesta) {

                    var listaAgenda = $("#listaAgenda");
                    $.each(respuesta.data, function(index, elemento) {
                        listaAgenda.append(
                            " <div  class='card-body'>" +
                            '<h5>' + elemento.nombre + '</h5>' +
                            "<button class='btn btn-info btn-sm'>" + '<b>' + 'FECHA : ' + '</b>' + elemento.fecha + '</button>' +
                            "<p class='card-text'>" + '<b>' + 'LUGAR : ' + '</b>' + elemento.lugar + '</p>' +
                            "<p class='card-title'>" + '<b>' + 'HORA : ' + '</b>' + '<small>' + elemento.hora + '</small>' + '</p>' +

                            '<div>'
                        );
                    });
                },
                error: function() {
                    console.log("No se ha podido obtener la información");
                }
            });


            $.ajax({
                url: 'aviso',
                type:'get',
                success: function(respuesta) {

                var listaAviso = $("#aviso");
                $.each(respuesta.data, function(index, elemento) {
                    console.log(elemento.imagen);
                    listaAviso.append(
                    elemento.imagen
                    );
                });
                },
                error: function() {
                console.log("No se ha podido obtener la información");
                }
            });
        });
        </script>
</body>

</html>