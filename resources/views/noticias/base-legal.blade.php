@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">

                <br>
                <center><h6 style="color:#007bff;">Base legal de las funciones de la DRVCS - APURIMAC</h6></center>
          <p> <b>Artículo 5º      Base Legal:Las normas en las que se sustentan  las funciones de la Dirección Regional Sectorial de Vivienda, Construcción y Saneamiento Apurímac, son las siguientes:
        </b>
       <br> <i class="fa fa-tint"></i> Ley Nº 27792, Ley de Organización y Funciones del MVCS.
      <br> <i class="fa fa-tint"></i> D. S. Nº 002-2002-VIVIENDA, Reglamento de Organización y Funciones - ROF del MVCS.
      <br> <i class="fa fa-tint"></i> D. S. Nº 005-2006-VIVIENDA, Plan Nacional de Vivienda – “Vivienda para Todos.
      <br> <i class="fa fa-tint"></i> Ley Nº 26338, Ley General de Servicios de Saneamiento.
      <br> <i class="fa fa-tint"></i> D. S. Nº 023-2005-VIVIENDA, Texto Único Ordenado del Reglamento de la Ley Nº 26338, Ley General de Servicios de Saneamiento.
      <br> <i class="fa fa-tint"></i> D. S. Nº 027-2003-VIVIENDA, Reglamento de Acondicionamiento Territorial y Desarrollo Urbano.
      <br> <i class="fa fa-tint"></i> D. S. Nº 012-2004-VIVIENDA, modificatoria del Reglamento de Acondicionamiento Territorial y Desarrollo Urbano.
      <br> <i class="fa fa-tint"></i> D. S. Nº 010-2006-VIVIENDA, TUO del Reglamento de la Ley General de Habilitaciones Urbanas.
      <br> <i class="fa fa-tint"></i> D. S. Nº 007-2006-VIVIENDA, Plan Nacional de Saneamiento 2006-2015 “Agua para la Vida.
      <br> <i class="fa fa-tint"></i> Ley Nº 27314, Ley General de Residuos Sólidos.
      <br> <i class="fa fa-tint"></i> Ley Nº 17752, Ley General de Aguas.
      <br> <i class="fa fa-tint"></i> Ley Nº 27783, Ley de Bases de la Descentralización.
      <br> <i class="fa fa-tint"></i> Ley Nº 27867, Ley Orgánica de Gobiernos Regionales y sus modificatorias.
      <br> <i class="fa fa-tint"></i> Ley Nº 28411, Ley General del Sistema Nacional de Presupuesto.
      <br> <i class="fa fa-tint"></i> Ley Nº 27658, Ley Marco de Modernización de la Gestión del Estado y su reglamento D. S. Nº 030-2002.PCM.
      <br> <i class="fa fa-tint"></i> D. S. Nº 043-2006-PCM, Lineamientos para la elaboración y aprobación del Reglamento de Organización y Funciones. ROF por parte de las entidades de la  Administración Pública.
      <br> <i class="fa fa-tint"></i> Ley N°28175, Ley Marco del Empleo Público
      <br> <i class="fa fa-tint"></i> Resolución Ministerial N°017-2013-VIVIENDA, declarar concluido el proceso de transferencia de la función establecida en
      el art. 58° literal g) de la Ley Orgánica de Gobiernos Regionales, en materia de vivienda y saneamiento a los Gobiernos Regionales de los departamentos de Apurímac, Ancash y Callao.

</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">

  <!-- Search Widget -->
  <div class="card my-4">
    <h6 class="card-header bg-primary" style="color:white">Buscar</h6>
    <div class="card-body">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="escribir...">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="button">ir!</button>
        </span>
      </div>
    </div>
  </div>

  <!-- Categories Widget -->
  <div class="card my-4">
    <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">
            <li>
              <a href="#">ATM</a>
            </li>
            <li>
              <a href="#">JASS</a>
            </li>
            <li>
              <a href="#">Actividades</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">
            <li>
              <a href="#">Eventos</a>
            </li>
            <li>
              <a href="#">Trámites</a>
            </li>
            <li>
              <a href="#">Directorio</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="card my-4">
    <ul class="list-group">
            <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
            <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
            <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
            <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
        </ul>
</div>

</div>

</div>

@endsection