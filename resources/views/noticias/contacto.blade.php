@extends('layout.templateblog')
@section('section')
<div class="row">


<div class="col-lg-8">

   <!--Section: Contact v.2-->
  <!--Section heading-->
  <h2 class="h1-responsive font-weight-bold text-center my-4">Contáctenos</h2>
  <!--Section description-->
  <p class="text-center w-responsive mx-auto mb-5">Tiene usted alguna pregunta? Por favor, no dude en ponerse en contacto con nosotros directamente. Estamos para servirle #AllinKausanapaq</p>

  <div class="row">
      <!--Grid column-->
      <div class="col-md-7 mb-md-0 mb-5">
          <form id="contact-form" name="contact-form">

              <!--Grid row-->
              <div class="row">

                  <!--Grid column-->
                  <div class="col-md-6">
                      <div class="md-form mb-0">
                          <input type="text" id="name" name="name" class="form-control">
                          <label for="name" style="font-size: 13px;color:#007bff;">TU NOMBRE</label>
                      </div>
                  </div>
                  <!--Grid column-->

                  <!--Grid column-->
                  <div class="col-md-6">
                      <div class="md-form mb-0">
                          <input type="text" id="email" name="email" class="form-control">
                          <label for="email" style="font-size: 13px; color:#007bff;">TU CORREO</label>
                      </div>
                  </div>
                  <!--Grid column-->

              </div>
              <!--Grid row-->

              <!--Grid row-->
              <div class="row">
                  <div class="col-md-12">
                      <div class="md-form mb-0">
                          <input type="text" id="subject" name="subject" class="form-control">
                          <label for="subject" style="font-size: 13px;color:#007bff;">ASUNTO</label>
                      </div>
                  </div>
              </div>
              <!--Grid row-->

              <!--Grid row-->
              <div class="row">

                  <!--Grid column-->
                  <div class="col-md-12">

                      <div class="md-form">
                          <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea "></textarea>
                          <label for="message" style="font-size: 13px;color:#007bff;">TU MENSAJE</label>
                      </div>

                  </div>
              </div>
              <!--Grid row-->

          </form>

          <div class="text-center text-md-right">
              <button class="btn btn-primary btn-sm">ENVIAR MENSAJE</button>
          </div>
          <div class="status"></div>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-md-5 text-center">
          <ul class="list-unstyled mb-0">
              <li><i class="fa fa-home fa-sm"></i>
                  <p>Jr. Lima 637, Abancay Apurímac 03001</p>
              </li>

              <li><i class="fa fa-phone fa-sm"></i>
                  <p>(083) 322837</p>
              </li>

              <li><i class="fa fa-envelope fa-sm"></i>
                  <p>drvcsapurimac@hotmail.com</p>
              </li>
          </ul>
      </div>
      <!--Grid column-->

  </div>
</div>
<div class="col-md-4">

<!-- Search Widget -->
<div class="card my-4">
  <h5 class="card-header">Buscar</h5>
  <div class="card-body">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="escribir...">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">ir!</button>
      </span>
    </div>
  </div>
</div>

<!-- Categories Widget -->
<div class="card my-4">
  <h5 class="card-header">Todo sobre</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#">ATM</a>
          </li>
          <li>
            <a href="#">JASS</a>
          </li>
          <li>
            <a href="#">Actividades</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#">Capacitaciones</a>
          </li>
          <li>
            <a href="#">Trámites</a>
          </li>
          <li>
            <a href="#">Directorio</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="card my-4">
  <ul class="list-group">
          <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
          <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
          <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
          <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
      </ul>
</div>
</div>
</div>
@endsection