@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">

    <div class="card my-4">

    <div class="card-body">
        <h5 class="card-header bg-primary" style="color:white">{{ $detalle->titulo }}</h5>
        <?php
      $orig =$detalle->contenido;

      $a = htmlentities($orig);

      $b = html_entity_decode($a);

      echo $b; // I'll "walk" the <b>dog</b> now
      ?>

<?php
$orig =$detalle->embed;

$a = htmlentities($orig);

$b = html_entity_decode($a);

echo $b; // I'll "walk" the <b>dog</b> now
?>
  @if (!is_null($detalle->archivo))
  <embed id="ca" src="{{ asset($detalle->archivo) }}" width="500" height="375"
  type="application/pdf">
  @endif
  <p class="lead">

  <center><img class="img-fluid rounded" src="{{asset($detalle->imagen)}}" width="500" height="400"></center>
  </p>

  <hr>
    <!-- Author -->
  <p class="lead" style="font-size:14px">
    <b><i class="fa fa-user"></i> Publicado por :</b>
    <a href="#">Imagen Institucional</a>  <i class="fa fa-calendar"></i> <b>Fecha: </b>{{$detalle->created_at}}
  </p>

  <!-- Date/Time -->
  <p style="font-size:14px"> </p>
</div>
</div>
</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">

  <!-- Search Widget -->
  <div class="card my-4">
    <h6 class="card-header bg-primary" style="color:white">Buscar</h6>
    <div class="card-body">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="escribir...">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="button">ir!</button>
        </span>
      </div>
    </div>
  </div>

  <!-- Categories Widget -->
  <div class="card my-4">
    <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">
            <li>
              <a href="#">ATM</a>
            </li>
            <li>
              <a href="#">JASS</a>
            </li>
            <li>
              <a href="#">Actividades</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">
            <li>
              <a href="#">Eventos</a>
            </li>
            <li>
              <a href="#">Trámites</a>
            </li>
            <li>
              <a href="#">Directorio</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="card my-4">
    <ul class="list-group">
            <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
            <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
            <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
            <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
        </ul>
</div>

</div>

</div>
@endsection