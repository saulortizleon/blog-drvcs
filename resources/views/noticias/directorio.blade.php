@extends('layout.templateblog')
@section('section')

<div class="row">
<div class="col-lg-8">
    <br>
    <center><h6 style="color:#007bff;">Directorio Institucional</h6></center>
    <p>Datos de los funcionarios y trabajadores de la DRVS APURIMAC  actualizados al presente año 2019</p>
    <div class="row">
        <div class="col-12">
          <div class="card mt-3 tab-card">
            <div class="card-header tab-card-header">
              <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="false">Dirección</a>
                </li>
                <li class="nav-item" class="active">
                    <a class="nav-link"  id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="true">Areas Tecnicas Municipales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">Facilitadores</a>
                </li>
              </ul>
            </div>

            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade  p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                <h5 class="card-title">Tab Card One</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the  content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>
              <div class="tab-pane fade show active p-3" id="two" role="tabpanel" aria-labelledby="two-tab" class="tab-pane active">
                  @foreach($directorio as $item)
                          <p class="card-text" style="font-size:14px">{{$item->titulo}}</p>
                          @if (!is_null($item->archivo))
                          <embed id="ca" src="{{ asset($item->archivo) }}" width="500" height="375"
                          type="application/pdf">
                          @endif

                  @endforeach
              </div>
              <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                <h5 class="card-title">Tab Card Three</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the  content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>

            </div>
          </div>
        </div>
      </div>
</div>
<div class="col-md-4">

<!-- Search Widget -->
<div class="card my-4">
  <h5 class="card-header">Buscar</h5>
  <div class="card-body">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="escribir...">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">ir!</button>
      </span>
    </div>
  </div>
</div>

<!-- Categories Widget -->
<div class="card my-4">
  <h5 class="card-header">Todo sobre</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#">ATM</a>
          </li>
          <li>
            <a href="#">JASS</a>
          </li>
          <li>
            <a href="#">Actividades</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#">Capacitaciones</a>
          </li>
          <li>
            <a href="#">Trámites</a>
          </li>
          <li>
            <a href="#">Directorio</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="card my-4">
  <ul class="list-group">
          <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
          <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
          <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
          <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
      </ul>
</div>
</div>
</div>
@endsection