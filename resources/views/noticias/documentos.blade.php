@extends('layout.templateblog')
@section('section')
<div class="row">
<div class="col-lg-12">
          <br>
    <h5>DOCUMENTOS PUBLICADOS POR LA DIRECCIÓN</h5>
    
<div class="table-responsive-lg">
  <table class="table">
    <thead class="thead">
      <tr>
        <th scope="col">Nombre documento</th>

        <th scope="col">Fecha</th>
        <th scope="col"> Acciones</th>
      </tr>
    </thead>
    @foreach ($documento as $item)
    <tbody>
      <tr>
        <td> {{$item->titulo}}</td>

        <td>{{$item->created_at}}</td>
        <th scope="row"><a href="{{asset($item->archivo)}}">Descargar</a></th>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

</div>


</div>
@endsection