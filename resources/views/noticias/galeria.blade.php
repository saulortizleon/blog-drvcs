@extends('layout.templateblog')
@section('section')
<div class="row">
<div class="col-md-12">
    <br>
    <center><h6 style="color:#007bff;">Galería de imágenes</h6></center>
    <p>Fotos de los últimos eventos y actividades realizados  con las Áreas Técnicas Municipales, JASS y en general, durante el presente año.</p>

    <div class="row">

     @foreach($galeria as $item)
    <div class="col-lg-6 col-sm-6 portfolio-item">
      <div class="card h-100">
          <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
        <div class="card-body">
          <p>{{ $item->titulo }}  <a type="button" class="btn btn-danger btn-sm" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"><i class="fa fa-eye"></i> Ver más</a></p>

                 </div>
      </div>
    </div>

    @endforeach

  </div>
</div>

</div>
@endsection
