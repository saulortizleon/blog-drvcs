@extends('layout.templateblog')
@section('section')
<br>
<center><h5 style="color:#007bff;">Nuestra Institución</h5> </center>
<div class="row">
  <div class="col-lg-6">
    <img class="img-fluid rounded mb-4" src="{{ asset('img/drvcs.jpg') }}" alt="">
  </div>
  <div class="col-lg-6">
    <center><h6 style="color:#007bff;">Misión</h6></center>
    <p>Contribuir a mejorar las condiciones de vida de la población,
     facilitando su acceso a una vivienda saludable y a los servicios
      de saneamiento básico de agua y desagüe, apoyar el crecimiento
      urbano ordenado de los centros poblados y mejoramiento del Medio
       Ambiente, así como propiciar la participación de grupos organizados.

      </p>
    </div>
</div>
<div class="row">

  <div class="col-lg-6">
    <center><h6 style="color:#007bff;">Visión</h6></center>
    <p>La Región Apurímac cuenta con un
    territorio ordenado, centros de población
     competitivos y sostenibles, así como los servicios básicos
      de saneamiento básico de agua y desagüe que brindan condiciones
      para el mejoramiento continuo de la calidad de vida de la población.</p>
    </div>
    <div class="col-lg-6">
      <img class="img-fluid rounded mb-4" src="{{ asset('img/equipo.jpg') }}" alt="">
    </div>
</div>
<div class="row">

  <div class="col-lg-12">

    <center><h6 style="color:#007bff;">Funciones</h6></center>
    <i class="fa fa-cog"></i> Facilitar el acceso a la población de ingresos medios y bajos a una vivienda adecuada mediante el desarrollo de acciones normativas y de gestión.

    <br><i class="fa fa-cog"></i> Promover la ocupación racional, ordenada y sostenible del Territorio Regional.

    <br><i class="fa fa-cog"></i> Fortalecer la investigación tecnológica y normalización de materiales, diseños y sistemas constructivos vinculados a programas de capacitación con los objetivos de industrialización comprendidas en el Plan Nacional de Vivienda.

    <br><i class="fa fa-cog"></i> Promover e impulsar el desarrollo equilibrado de las actividades sectoriales para preservar la calidad sanitaria y del medio ambiente.

</div>

@endsection