@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">
    <br>

    <center><h5 style="color:#007bff;">Plan Regional de Saneamiento de Apurímac (2018 – 2021)</h5></center>
    <center><h6 style="color:#007bff;">  Presentación </h6></center>
    <p class="text-justify">
      El Gobierno del Perú, priorizo como objetivo principal en el sector saneamiento, el dotar
del acceso a los servicios de saneamiento en el ámbito urbano al 2021 y lograr la
universalización de estos servicios en forma sostenible antes del año 2030, cumpliendo
así con los objetivos de desarrollo sostenible de las naciones unidas, a los cuales el
Perú se ha adherido, en esa dirección el Gobierno Regional de Apurímac, de acuerdo
con los Objetivos de Desarrollo Sostenible que están expresados en el Plan Nacional
de Saneamiento 2017 - 2021 y en el Plan Bicentenario Perú al 2021, ha elaborado El
Plan Regional de Saneamiento 2018 - 2021 <br><br>
El Plan Regional de Saneamiento de la Región Apurímac es el resultado de un proceso
de trabajo concertado entre los representantes de las Municipalidades Distritales - ATM,
Sectores vinculados al tema de saneamiento como el Centro de Atención al Cliente –
CAC del MVCS, DIRESA, Autoridad Nacional del Agua - ANA, SUNASS, entre otros,
con la participación activa de operadores del servicio como UGM, JASS, EPS, liderada
por el Gobierno Regional de Apurímac a través de la Dirección Regional de Vivienda,
Construcción y Saneamiento de Apurímac, en el marco de la Política y Plan Nacional de
Saneamiento; la participación del MVCS ha sido fundamental por el apoyo y asistencia
técnica a los miembros del Equipo Técnico de Trabajo - ETT.<br><br>
El Plan Regional de Saneamiento cuenta con la opinión favorable de la Dirección
Nacional de Saneamiento del MVCS; y la aprobación a través de la Resolución Ejecutiva
Regional N° 237 - 2018 – GR Apurímac / GR, de fecha 28 de junio del 2018. En adelante,
la implementación del presente Plan Regional de Saneamiento de Apurímac 2018 -
2021, requiere también la participación de todos los actores constituidos en la
COMURSABA <br><br>
El Gobierno Regional y La Dirección Regional de Saneamiento de Apurímac gestión
2015 - 2018, nos sentimos complacidos por poner al alcance del pueblo este importante
instrumento de gestión que busca no solo cerrar las brechas de infraestructura de
saneamiento (cobertura), también un servicio de calidad y sostenido de saneamiento en
Apurímac, se constituye en un instrumento de gestión y de cumplimiento obligatorio en
el marco de las políticas públicas priorizadas del Perú al 2021.
    </p>
      <center><h6 style="color:#007bff;">  Objetivo Principal </h6></center>
      <p>
        Alcanzar el acceso universal, sostenible y de calidad a los servicios de
        saneamiento en la Región Apurímac
      </p>
     <center> <h6 style="color:#007bff;">Objetivos Específicos</h6></center>
     <p>
    <i class="fa fa-tint"></i> Atender a la población sin acceso a los servicios y de manera prioritaria a la de escasos recursos.
     <br><i class="fa fa-tint"></i> Desarrollar y fortalecerla capacidad de gestión de los prestadores.
     <br><i class="fa fa-tint"></i> Desarrollar proyectos de saneamiento sostenibles, con eﬁciencia técnica, administrativa,económica y ﬁnanciera.
     <br><i class="fa fa-tint"></i> Consolidar el rol rector del GR - DRVCS y fortalecer la articulación con los actores involucrados en el sector saneamiento.
     <br><i class="fa fa-tint"></i> Desarrollar una cultura ciudadana de valoración de los servicios de saneamiento.
    </p>
    <center> <h6 style="color:#007bff;">Base Legal</h6></center>
    <p>
      <i class="fa fa-cog"></i> Ley N° 27867, Ley Orgánica de Gobiernos Regionales.
      <br><i class="fa fa-cog"></i> Ley Mecanismos de Retribución de Servicios Eco sistémicos - MRSE Ley No.
30215 – MINAM y Decreto supremo No. 009-2016-MINAM, que aprueba
reglamento de ley de mecanismos de retribución de servicios eco sistémicos
Decreto Supremo N° 027-2017-EF, que aprueba el Reglamento del Decreto
Legislativo N° 1252, Decreto Legislativo que crea el Sistema Nacional de
Programación Multianual y Gestión de Inversiones y deroga la Ley N° 27293,
Ley del Sistema Nacional de Inversión Pública.
<br><i class="fa fa-cog"></i> Decreto Supremo N° 007-2017-VIVIENDA, Decreto Supremo que aprueba la
Política Nacional de Saneamiento.
<br><i class="fa fa-cog"></i> Decreto Supremo N° 018-2017-VIVIENDA, Decreto Supremo que aprueba el
Plan Nacional de Saneamiento 2017-2021.
<br><i class="fa fa-cog"></i> Decreto Supremo N° 019-2017-VIVIENDA, Decreto Supremo que aprueba el
Reglamento del Decreto Legislativo N° 1280, Decreto Legislativo que aprueba la
Ley Marco de la Gestión y Prestación de los Servicios de Saneamiento.
<br><i class="fa fa-cog"></i> Decreto Supremo N° 056-2018-PCM, que aprueba la Política General de
Gobierno al 2021.
<br><i class="fa fa-cog"></i>  Decreto Legislativo N° 1252, Decreto Legislativo que crea el Sistema Nacional
de Programación Multianual y Gestión de Inversiones y deroga la Ley N° 27293,
Ley del Sistema Nacional de Inversión Pública.
<br><i class="fa fa-cog"></i> Decreto Legislativo N° 1280, Decreto Legislativo que aprueba la Ley Marco de
la Gestión y Prestación de los Servicios de Saneamiento.
<br><i class="fa fa-cog"></i> Resolución Ministerial N° 336-2014-VIVIENDA, que aprueba el Plan de
Inversiones del Sector Saneamiento de alcance nacional 2014-2021.
<br><i class="fa fa-cog"></i> Con Resolución Ministerial Nº 384-2017-VIVIENDA se aprueban los
Lineamientos para la formulación, aprobación, seguimiento y evaluación de los
Planes Regionales de Saneamiento.
<br><i class="fa fa-cog"></i> Con Resolución de Presidencia de Consejo Directivo Nº 00027-
2018/CEPLAN/PCD Aprueban “Módulo de seguimiento de la Política General de
Gobierno al 2021” del aplicativo CEPLAN V.01.
<br><i class="fa fa-cog"></i> Ordenanza Regional No. 005-2017-GR Apurímac/CR, declara de interés y
prioridad regional la lucha contra la desnutrición crónica infantil en niños menores
de 5 años y de la anemia en menores de 6 a 36 meses.
<br><i class="fa fa-cog"></i>  Ordenanza Regional No 032-2012 – GR Apurímac/CR, que crea el sistema de
información regional de agua y saneamiento de Apurímac.
<br><i class="fa fa-cog"></i> Ordenanza Regional No. 013-2012 – GR Apurímac/CR, aprueba lineamientos de
política para la gestión de agua y saneamiento de obligatorio cumplimiento en el
ámbito del gobierno regional de Apurímac, para toda intervención que se haga
cualquiera fuera el fondo de financiamiento.
    </p>
</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
    <!-- Search Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white">Buscar</h6>
      <div class="card-body">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="escribir...">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="button">ir!</button>
          </span>
        </div>
      </div>
    </div>
    <!-- Categories Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">ATM</a>
              </li>
              <li>
                <a href="#">JASS</a>
              </li>
              <li>
                <a href="#">Actividades</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">Eventos</a>
              </li>
              <li>
                <a href="#">Trámites</a>
              </li>
              <li>
                <a href="#">Directorio</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Side Widget -->
    <div class="card my-4">
      <ul class="list-group">
              <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
              <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
              <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
              <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
          </ul>
  </div>
</div>

</div>

@endsection