@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">
    <br>
    <center><h5 style="color:#007bff;">ROMAS APURÍMAC</h5></center>
    <center><p> OPERACIÓN Y MANTENIMIENTO DEL SISTEMA DE AGUA Y SANEAMIENTO EN LA REGION APURIMAC” </b><br>   </center>

    <center><h6 style="color:#007bff;"><i class="fa fa-tint"></i> Objetivo</h6></center>
      <center>  <p>Es regular la labor a desarrollar durante el proceso de preparación, ejecución física y de
            la liquidación de las actividades de mantenimiento, Reposición, Operación y
            Mantenimiento del Sistema de Agua y Saneamiento en la Región de Apurímac.</p> </center>


    <center><h6 style="color:#007bff;"><i class="fa fa-tint"></i> Finalidad</h6></center>

<p class="text-justify">
        Establecer los lineamientos, procedimientos, funciones y obligaciones que garanticen la
        adecuada ejecución de las actividades de mantenimiento, reposición, operación y
        mantenimiento del sistema de agua y saneamiento en la Región Apurímac,
        estableciendo las responsabilidades correspondientes, de acuerdo con las normas y
        procedimientos que regulan la ejecución y control de los proyectos de inversión pública,
        vigente.
</p>

<center><h6 style="color:#007bff;"> <i class="fa fa-tint"></i> Alcance</h6></center>
<p class="text-justify">
    La presente Directiva es de aplicación para los órganos estructurales y dependencias que
        conforman el pliego del Gobierno Regional de Apurímac, involucrados en el proceso de
        rehabilitación, Reposición, Operación y Mantenimiento del Sistema de Agua y
        Saneamiento en la Región de Apurímac
 </p>
 <center><h6 style="color:#007bff;"><i class="fa fa-tint"></i> Base Legal</h6></center>
 <p>
        <i class="fa fa-check-circle"></i>Ley N° 27867, Ley Orgánica de los Gobiernos regionales y sus Modificatorias.
        <br><i class="fa fa-check-circle"></i>Ley N° 27658, Ley marco de la Modernización de la gestión del Estado, y sus
        normas modificatorias (Leyes N° 27842,27852 y 27899).
        <br><i class="fa fa-check-circle"></i> Ley N° 27506, Ley del Canon y sus respectivas modificatorias (Ley N° 28077)
        <br><i class="fa fa-check-circle"></i> Ley N° 30225, Ley de Contrataciones del Estado y su modificatoria Decreto
        legislativo N° 1341.
        <br><i class="fa fa-check-circle"></i> Ley N° 29622, Ley que modifica la Ley N° 27785, ley Orgánica del Sistema
        nacional de Control y de la Contraloría General de la Republica.
        <br><i class="fa fa-check-circle"></i> Resolución de Contraloría General N° 320-2006-C.G, que aprueba las normas de
        Control Interno.
        <br><i class="fa fa-check-circle"></i> Decreto Legislativo No 1280 Ley marco de la gestión y prestación de los servicios
        de saneamiento.
        <br><i class="fa fa-check-circle"></i> Decreto Supremo No 018-2017-VIVIENDA, que aprueba el Plan Nacional de
        Saneamiento 2017 – 2021
        <br><i class="fa fa-check-circle"></i> Ordenanza Regional No 013-2012-GR/APURIMAC/CR, aprobación de aprobar, la
        creación del sistema de información de agua y saneamiento – SIAS Apurímac.
        <br><i class="fa fa-check-circle"></i> Ordenanza Regional Na 013-2012-GR/APURIMAC/CR, aprueba lineamientos de
        política para la Gestión del agua y saneamiento de obligatorio cumplimiento en el
        ámbito del Gobierno Regional de Apurímac para toda intervención que se haga,
        cualquiera fuera el fondo de financiamiento.
        <br><i class="fa fa-check-circle"></i> Resolución Ejecutiva Regional Na 237-2018, Aprueba el Plan Regional de
        Saneamiento.
  </p>
</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
    @foreach($romas as $item)
    <div class="card my-4">
            <div class="card-body">
          <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>

          <p>{{ $item->titulo }}
               <a type="button" class="btn btn-link btn-sm"
               href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
               <i class="fa fa-hand-pointer-o"></i> Ver más</a></p>
            </div>
      </div>
@endforeach
      <center><a type="button" class="btn btn-primary btn-sm"
        href="{{url('proyectos-romas')}}">
        <i class="fa fa-list-ol"></i> Ver todos los proyectos</a></center>

    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">ATM</a>
              </li>
              <li>
                <a href="#">JASS</a>
              </li>
              <li>
                <a href="#">Actividades</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">Eventos</a>
              </li>
              <li>
                <a href="#">Trámites</a>
              </li>
              <li>
                <a href="#">Directorio</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Side Widget -->
    <div class="card my-4">
      <ul class="list-group">
              <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
              <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
              <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
              <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
          </ul>
  </div>
</div>

</div>

@endsection