@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">
    <br>
    <center><h5 style="color:#007bff;">Dirección de Construcción y Saneamiento</h5></center>
    <p>  Ejerce las siguientes <b> Funciones: </b><br>
    <i class="fa fa-check-circle"></i> Formular, coordinar, ejecutar y evaluar los planes y políticas regionales, en materia de construcción, sanemainto y medio ambiente, referidas al sector.
    <br><i class="fa fa-check-circle"></i> Cumplir y cautelar el cumplimiento de la normatividad, en materia de construcción, saneamiento y medio ambiente, en el ámbito de su competencia.
    <br><i class="fa fa-check-circle"></i> Promover y gestar programas, proyectos y estudios de construcción, de infraestructura y saneamiento, ambientalmente equilibrados, en beneficio de la población regional, en coordinación con los Gobiernos Locales.
    <br><i class="fa fa-check-circle"></i> Estimular la participación de la iniciativa de la inversión privada en la generación de la oferta de construcción de ingraestructura y servicios de saneamiento urbano y rural, articulando en el ámbito regional con las instancias vinculadas.
    <br><i class="fa fa-check-circle"></i> Promover el desarrollo de las inversiones en la construcción de infraestructura, de acuerdo a la normatividad vigente.
    <br><i class="fa fa-check-circle"></i> Conducir la elaboración del diagnóstico situacional en materia de saneamiento y sus necesidades en el ámbito regional.
    <br><i class="fa fa-check-circle"></i> Gestionar la identificación de fuentes de financiamiento, provenientes de Cooperación Técnica Internacional, en apoyo a la formulación y ejecución de los proyectos de saneamiento a cargo de los Gobiernos Locales.
    <br><i class="fa fa-check-circle"></i> Proporcionar asistencia técnica especializada a los Gobiernos Locales, en el desarrollo de los proyectos de inversión en materia de saneamiento, cuando así lo soliciten.
    <br><i class="fa fa-check-circle"></i> Coordinar los planes y acciones, previas o posteriores, en casos de desastres, que le corresponde asumir a la Dirección Regional Sectorial, conforme a las disposiciones que le corresponda asumir.
</p>
<p>
    <center><h6 style="color:#007bff;"> Objetivos en Construcción</h6></center>

    <i class="fa fa-wrench"></i> Promover, coordinar, formular y/o conducir programas de asistencia técnica para el desarrollo de programas habitacionales dirigidos a familias de menores recursos.
    <br><i class="fa fa-wrench"></i> Incentivar la difusión y las capacitaciones tecnológicas con fines de innovación sobre normatividad y sistemas de construcción nuevas.
    <br><i class="fa fa-wrench"></i> Estimular el estudio y mejoramiento de tecnologías constructivas tradicionales en lo que respecta a condiciones sismo resistente, de estabilización, confort, durabilidad y seguridad.
    <br>
</p>
<center><h6 style="color:#007bff;"> Objetivos en Saneamiento</h6></center>
<p>

    <i class="fa fa-tint"></i> Contribuir a ampliar la cobertura y mejorar la calidad y sostenibilidad de los servicios de agua potable, alcantarillado, tratamiento de aguas servidas y disposición de excretas.
    <br><i class="fa fa-tint"></i> Promover la sostenibilidad de los sistemas, la ampliación de la cobertura y el mejoramiento de la calidad de los servicios de saneamiento en el ámbito urbano y rural.
</p>
</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
    <!-- Search Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white">Buscar</h6>
      <div class="card-body">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="escribir...">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="button">ir!</button>
          </span>
        </div>
      </div>
    </div>
    <!-- Categories Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">ATM</a>
              </li>
              <li>
                <a href="#">JASS</a>
              </li>
              <li>
                <a href="#">Actividades</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">Eventos</a>
              </li>
              <li>
                <a href="#">Trámites</a>
              </li>
              <li>
                <a href="#">Directorio</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Side Widget -->
    <div class="card my-4">
      <ul class="list-group">
              <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
              <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
              <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
              <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
          </ul>
  </div>
</div>

</div>

@endsection