@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">
    <br>
    <center><h5 style="color:#007bff;">Dirección de Vivienda y Urbanismo</h5></center>
    <p>  Ejerce las siguientes <b>Funciones:</b>  <br>
    <i class="fa fa-arrow-circle-right"></i> Formular y ejecutar los planes y políticas regionales, en materia de vivienda y urbanismo.
    <br><i class="fa fa-arrow-circle-right"></i> Promover y gestar programas y proyectos de vivienda y de desarrollo urbano, ambientamente equilibrados, en beneficio de la población y del hábitat, en coordinación con los Gobiernos Locales.
    <br><i class="fa fa-arrow-circle-right"></i> Coordinar la participación de la iniciativa e inversión privada en la generación de la Oferta de Vivienda, en el ámbito regional.
    <br><i class="fa fa-arrow-circle-right"></i> Conducir la elaboración del Diagnóstico Situacional, en materia de Vivienda y Urbanismo, bajo su competencia.
    <br><i class="fa fa-arrow-circle-right"></i> Gestionar las acciones que la Ley prevé para la prestación o captación de Proyectos de Vivienda y Urbanismo, a cargo de la DRVCS - Apurimac.
    <br><i class="fa fa-arrow-circle-right"></i> Programar y gestionar la solicitud de los requerimientos financieros, que sean necesarios para la ejecución de los proyectos bajo su responsabilidad.
     </p>
     <p>

     <center><h6 style="color:#007bff;">Objetivos en Vivienda</h6></center>
     <i class="fa fa-wrench"></i> Promover el mejoramiento de viviendas existentes en el ámbito urbano y rural.
      <br><i class="fa fa-wrench"></i> Coordinar, formular y/o conducir programas de asistencia técnica para el desarrollo de programas habitacionales dirigidos a las familias de menores recursos.
      <br><i class="fa fa-wrench"></i> Promover mecanismos de financiamiento para que la población pobre y extremadamente pobre pueda acceder a una vivienda.
      <br>
      <br>
      <center><h6 style="color:#007bff;"> Objetivos en Urbanismo</h6></center>

      <i class="fa fa-cog"></i> Contribuir gradualmente a la organización del terreno y consolidación del sistema urbano regional.
      <br><i class="fa fa-cog"></i> Apoyar a la elaboracion del plan de  Acondicionamiento Territorial de la Región de Apurímac en coordinación con los gobiernos locales.
      <br><i class="fa fa-cog"></i> Facilitar el acceso al suelo urbanizado para vivienda desalentando la ocupación informal.
      <br><i class="fa fa-cog"></i> Formular propuestas de acondicionamiento del espacio, zonificación economica - ecológica y programas puntuales de vivienda, equipamiento urbano y productivo para el desarrollo y consolidación del sistema urbano -territorial.
      <br><i class="fa fa-cog"></i> Brindar asistencia técnica a gobiernos locales en gestión de territorio.
      <br><i class="fa fa-cog"></i> Consolidar el proceso de integración física y social de los barrios urbanos marginales.
      <br><i class="fa fa-cog"></i> Identificar, proponer e impulsar; de manera coordinada y concertada, la dotación o complementación de infraestructura urbana.
     </p>
</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
    <!-- Search Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white">Buscar</h6>
      <div class="card-body">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="escribir...">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="button">ir!</button>
          </span>
        </div>
      </div>
    </div>
    <!-- Categories Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">ATM</a>
              </li>
              <li>
                <a href="#">JASS</a>
              </li>
              <li>
                <a href="#">Actividades</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">Eventos</a>
              </li>
              <li>
                <a href="#">Trámites</a>
              </li>
              <li>
                <a href="#">Directorio</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Side Widget -->
    <div class="card my-4">
      <ul class="list-group">
              <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
              <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
              <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
              <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
          </ul>
  </div>
</div>

</div>

@endsection