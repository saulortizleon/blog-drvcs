@extends('layout.templateblog')
@section('section')
<div class="row">

<!-- Post Content Column -->
<div class="col-lg-8">
    <br>
    <center><h5 style="color:#007bff;">Videos Informativos </h5></center>

            <p>Explicación detallada del sistema de cloración por goteo mejorado, durante el VII ENCUENTRO DE JASS APURIMAC</p>
    <iframe width="525" height="315" src="https://www.youtube.com/embed/vURc_Xkv-Dk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   <br>   <p>VII encuentro JASS APURIMAC, realizado en la ciudad de Abancay los días 12 y 13 de noviembre del 2019</p>
    <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fdrvcsapurimacoficial%2Fvideos%2F1642341372557156%2F&show_text=0&width=525"
     width="525" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
</div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
    <!-- Search Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white">Buscar</h6>
      <div class="card-body">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="escribir...">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="button">ir!</button>
          </span>
        </div>
      </div>
    </div>
    <!-- Categories Widget -->
    <div class="card my-4">
      <h6 class="card-header bg-primary" style="color:white" >Todo sobre</h5>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">ATM</a>
              </li>
              <li>
                <a href="#">JASS</a>
              </li>
              <li>
                <a href="#">Actividades</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#">Eventos</a>
              </li>
              <li>
                <a href="#">Trámites</a>
              </li>
              <li>
                <a href="#">Directorio</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Side Widget -->
    <div class="card my-4">
      <ul class="list-group">
              <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
              <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
              <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
              <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
          </ul>
  </div>
</div>

</div>

@endsection