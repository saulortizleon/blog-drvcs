<?php

Route::group(['middleware'=>['admin']], function()
{
    Route::get('adm','AdminController@admin');
    Route::get('adm/noticias','AdminController@adminNoticias');
    //noticias insert e edit
    Route::get('noticias/insertar','AdminController@insertarNoticia');
    Route::post('noticias/insertar','AdminController@insertarNoticia');

    Route::post('noticias/editar','AdminController@editarNoticia');
    Route::get('noticias/editar/{idPublicacion}','AdminController@editarNoticia');

    Route::post('adm/agenda','AdminController@insertarAgenda');

    Route::post('adm/archivos','AdminController@insertarArchivos');
    Route::get('adm/archivos','AdminController@adminArchivos');
    //agenda
    Route::get('adm/agenda','AdminController@adminAgenda');

    Route::get('agenda/edit/{idAgenda}','AdminController@editarAgenda');
    Route::post('agenda/edit','AdminController@editarAgenda');
    //ELIMINAR
    Route::get('agenda/eliminar/{idAgenda}','AdminController@eliminarAgenda');
    Route::get('noticia/eliminar/{idPublicacion}','AdminController@eliminarNoticia');
    Route::get('archivo/eliminar/{idDoc}','AdminController@eliminarArchivo');

    Route::get('adm/login','LoginController@actionLogOut');

});
//sitemap
Route::get('/sitemap.xml', 'SitemapController@sitemap');
//todo avisos
Route::get('aviso','NoticiasController@listaAviso');

//todo noticias

Route::get('noticias','NoticiasController@listaNoticias');
Route::get('documentos','NoticiasController@listaDocumentos');
Route::get('agenda','NoticiasController@agenda');
Route::get('documento','NoticiasController@documento');
Route::get('noticias/detalle','NoticiasController@detalle');
Route::get('/','NoticiasController@index');
Route::get('noticias/detalle/{id}','NoticiasController@detalle');
//informativos

Route::get('institucional','NoticiasController@institucional');
Route::get('documento','NoticiasController@documento');
Route::get('contacto','NoticiasController@contacto');
Route::get('oficinas','NoticiasController@oficinas');
Route::get('transparencia','NoticiasController@transparencia');
Route::get('galeria','NoticiasController@verGaleria');
Route::get('romas','NoticiasController@romas');
Route::get('directorio','NoticiasController@directorio');
route::get('plan-regional-saneamiento-apurimac','NoticiasController@plan');
//paginas principales
Route::get('vivienda-urbanismo', function () {
    return view('noticias/urbanismo');
});
Route::get('construccion-saneamiento', function () {
    return view('noticias/saneamiento');
});
Route::get('funciones', function () {
    return view('noticias/funciones');
});

Route::get('contacto', function () {
    return view('noticias/contacto');
});

Route::get('base-legal', function () {
    return view('noticias/base-legal');
});
Route::get('vivo', function () {
    return view('noticias/vivo');
});
Route::get('videos', function () {
    return view('noticias/videos');
});
//todo login
Route::get('adm/login','LoginController@ver');
Route::post('adm/login','LoginController@actionLogIn');






